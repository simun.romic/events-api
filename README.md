# Project events-api

## Project dependencies

- this project uses Node v12 and AWS Lambda for code execution
- this project uses serverless framework for deployment on specific stage. Each 'service' defined in `services` folder has it's own deployment script `serverless.yml` where specific informations are defined
- for each service NPM dependencies should be installed by running: `npm i` from each `services` subfolder, defined in `package.json` file
- also, there is common `package.json` file in root folder, for all common project dependencies and those should be installed as well

- there are 3 stages, which can be specified both in each `serverless.yml` file under property `provider.stage` or as argument when running serverless from command line: `--stage ${STAGE}`

  - dev - default
  - qa
  - prod

## Project deployment process

- as some services are using ARN references from other services (exported using serverless framework), there is order for deployment process for this services

  - services deployment order:
    - databases - e.g. `npm run deploy:db:${STAGE}`
    - authorization-api - e.g. `npm run deploy:authorizationApi:${STAGE}`
    - events-api - e.g. `npm run deploy:eventsApi:${STAGE}`

- to remove service from specific stage, run:

  - databases - e.g. `npm run delete:db:${STAGE}`
  - authorization-api - e.g. `npm run delete:authorizationApi:${STAGE}`
  - events-api - e.g. `npm run delete:eventsApi:${STAGE}`

- `${STAGE}` is one of above values: dev/qa/prod
