// Help function to generate an IAM policy

/**
 * Generate allowed method arns for every method under given path
 * This enables all APIs to be invoked with same Authorization token
 * because generated policy is cached on first API call and
 * any other call with same Authorization token with be denied
 * @param {String} methodArn
 * @param {String} methodPath
 */
const generatemethodArns = (methodArn, allowAll) =>
  allowAll
    ? `${methodArn
        .split('/')
        .slice(0, 2)
        .join('/')}/*`
    : methodArn;

const generateIAMPolicy = (principalId, effect, resource, allowAll = false) => {
  const policy = {};

  policy.principalId = principalId;
  if (effect && resource) {
    const policyDocument = {};
    policyDocument.Version = '2012-10-17';
    policyDocument.Statement = [];
    const statementOne = {};
    statementOne.Action = 'execute-api:Invoke';
    statementOne.Effect = effect;
    statementOne.Resource = generatemethodArns(resource, allowAll);
    policyDocument.Statement[0] = statementOne;
    policy.policyDocument = policyDocument;
  }

  return policy;
};

export default generateIAMPolicy;
