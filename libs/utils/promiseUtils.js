import EnrichedError from '../errors/EnrichedError';
import { isNull, isNonNullOrEmpty, deepCopy } from './objectUtils';

/**
 * Wrap Promise to safe Promise
 * Catch exeception and return error instance instead of throwing it
 * @param {Promise} p
 * @param {Boolean} showErrorLog
 * @returns {Promise}
 */
const safeExecution = async (p, showErrorLog = true) => {
  try {
    return await p;
  } catch (error) {
    if (showErrorLog) {
      console.error(`Error while executing promise with error: ${error}`);
    }
    return error;
  }
};

/**
 * Add additional data to error
 * @param {Promise} p
 * @param {Object} contextData
 * @returns {Promise}
 * @throws {Error}
 */
const failWithContextData = async (p, contextData) => {
  try {
    return await p;
  } catch (error) {
    const newContextData = deepCopy(isNull(contextData) ? {} : contextData);

    // if error from HTTP service
    if (isNonNullOrEmpty(error.response)) {
      const statusCode = error.response.status;
      newContextData.statusCode = statusCode;
    }

    const enrichedError = new EnrichedError(error.message, newContextData);

    throw enrichedError;
  }
};

/**
 *
 * @param {Number} time // time in ms
 * @returns {Promise<void>}
 */
const sleep = async time => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, time);
  });
};

export { safeExecution, failWithContextData, sleep };
