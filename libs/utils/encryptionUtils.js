import { randomBytes, createCipheriv, createDecipheriv } from 'crypto';

import { isNull } from './objectUtils';

const AES_IV_BYTES = 16;
const ALOGRITHM = 'AES-256-CBC';

/**
 * Encript data with crypto lib
 * @param {String} key
 * @param {String} text
 * @returns {String}
 */
const encryptAES = (key, text) => {
  const iv = randomBytes(AES_IV_BYTES);
  const keyBuffer = Buffer.from(key, 'hex');
  const textBuffer = Buffer.from(text, 'utf8');

  const cipher = createCipheriv(ALOGRITHM, keyBuffer, iv);
  const encryptedBuffer = Buffer.concat([cipher.update(textBuffer), cipher.final()]);

  return `${iv.toString('hex')}:${encryptedBuffer.toString('hex')}`;
};

/**
 * Decript data with crypto lib
 * @param {String} key
 * @param {String} text
 * @param {String} iv
 * @returns {String}
 */
const decryptAES = (key, text, iv) => {
  const ivBuffer = Buffer.from(iv, 'hex');
  const keyBuffer = Buffer.from(key, 'hex');
  const textBuffer = Buffer.from(text, 'hex');

  const decipher = createDecipheriv(ALOGRITHM, keyBuffer, ivBuffer);
  const decryptedBuffer = Buffer.concat([decipher.update(textBuffer), decipher.final()]);

  return decryptedBuffer.toString('utf8');
};

/**
 * Ecrypt given text with given key using crypto lib
 * @param {String} key
 * @param {String}} text
 * @returns {String}
 */
const encrypt = (key, text) => {
  if (isNull(key) || isNull(text)) {
    throw new Error('Key and text must be non null or empty');
  }

  return encryptAES(key, text);
};

/**
 * Decrypt given encrypted text with given key using crypto lib
 * @param {String} key
 * @param {String} encryptedText
 * @return {String}
 */
const decrypt = (key, encryptedText) => {
  if (isNull(encryptedText) || isNull(key)) {
    throw new Error('Key and encryptedText must be non null or empty');
  }

  const [iv, text] = encryptedText.split(':');
  return decryptAES(key, text, iv);
};

export { encrypt, decrypt };
