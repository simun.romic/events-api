/**
 * Check if given object empty of null
 * @param {Object} obj
 * @returns {Boolean}
 */
const isEmptyOrNull = obj => {
  return !obj || Object.getOwnPropertyNames(obj).length === 0;
};

/**
 * Check if given object is null or undefined
 * @param {Object} obj
 * @returns {Boolean}
 */
const isNull = obj => {
  return !obj;
};

/**
 * Check if given object is non empty or null
 * @param {Object} obj
 */
const isNonNullOrEmpty = obj => !isEmptyOrNull(obj);

/**
 * Create deep copy of given object
 * returns new object
 * @param {Object} obj
 * @returns {Object}
 */
const deepCopy = obj => {
  if (isNull(obj)) {
    throw new Error('Object can not be null or empty when making deep copy');
  }
  return JSON.parse(JSON.stringify(obj));
};

export { isEmptyOrNull, isNull, isNonNullOrEmpty, deepCopy };
