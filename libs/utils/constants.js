const CONTENT_TYPE = Object.freeze({
  APPLICATION_JSON: 'application/json',
  PLAIN_TEXT: 'text/plain',
  APPLICATION_PDF: 'application/pdf',
});

const STATUS_CODE = Object.freeze({
  OK: 200,
  CREATED: 204,
  INTERNAL_SERVER_ERROR: 500,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  REDIRECT: 301,
  NOT_FOUND: 404,
});

const HTTP_REQUEST_METHOD = Object.freeze({
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  PATCH: 'PATCH',
  DELETE: 'DELETE',
  HEAD: 'HEAD',
  OPTIONS: 'OPTIONS',
});

/**
 * NONE - If ReturnValues is not specified, or if its value is NONE, then nothing is returned.
 * ALL_OLD - Returns all of the attributes of the item, as they appeared before update operation.
 * UPDATED_OLD - Returns only the updated attributes, as they appeared before update operation.
 * ALL_NEW - Returns all of the attributes of the item, as they appear after update operation.
 * UPDATED_NEW - Returns only the updated attributes, as they appear after the update operation.
 */
const DDB_RETURN_VALUES = Object.freeze({
  NONE: 'NONE',
  ALL_OLD: 'ALL_OLD',
  UPDATED_OLD: 'UPDATED_OLD',
  ALL_NEW: 'ALL_NEW',
  UPDATED_NEW: 'ALL_NEW',
});

export { CONTENT_TYPE, STATUS_CODE, HTTP_REQUEST_METHOD, DDB_RETURN_VALUES };
