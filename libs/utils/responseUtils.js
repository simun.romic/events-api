/**
 *
 * @param {Number} statusCode
 * @param {Object} headers
 * @param {Object} data
 * @returns {Object}
 */
const generateResponse = (statusCode, headers, data) => {
  const responseData = data ? JSON.stringify(data) : '';
  return {
    statusCode,
    headers,
    body: responseData,
  };
};

export default generateResponse;
