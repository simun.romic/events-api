// eslint-disable-next-line import/no-extraneous-dependencies
import AWSXRay from 'aws-xray-sdk-core';

// eslint-disable-next-line import/no-extraneous-dependencies
import DynamoDB from 'aws-sdk/clients/dynamodb';

class GenericRepository {
  /**
   *
   * @param {DocumentClient} dbClient
   */
  constructor() {
    const ddbClient = AWSXRay.captureAWSClient(new DynamoDB());

    this.dbClient = new DynamoDB.DocumentClient({
      service: ddbClient,
    });
    this.dbClient.service = ddbClient;
  }

  /**
   * Insert item into table
   * @param {Object} params
   * @returns {Promise}
   */
  async create(params) {
    return this.dbClient.put(params).promise();
  }

  /**
   * Get item from table
   * @param {Object} params
   * @returns {Promise}
   */
  async get(params) {
    return this.dbClient.get(params).promise();
  }

  /**
   * Delete item from table
   * @param {Object} params
   * @returns {Promise}
   */
  async delete(params) {
    return this.dbClient.delete(params).promise();
  }

  /**
   * Update item in table
   * @param {Object} params
   * @returns {Promise}
   */
  async update(params) {
    return this.dbClient.update(params).promise();
  }

  /**
   * Query item against GSI
   * @param {Object} params
   * @returns {Promise}
   */
  async query(params) {
    return this.dbClient.query(params).promise();
  }
}

export default GenericRepository;
