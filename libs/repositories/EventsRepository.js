import uuid from 'uuid/v4';

import { DDB_RETURN_VALUES } from '../utils/constants';

import GenericRepository from './GenericRepository';

class EventsRepository extends GenericRepository {
  constructor(table) {
    super();

    this.table = table;
  }

  /**
   *
   * @param {Object} event
   * @param {String} userId
   * @returns {Promise<Object>}
   */
  async createEvent(event, userId) {
    const createdAt = new Date().getTime();
    const item = {
      UserId: userId,
      UUID: uuid(),
      ...event,
      createdAt,
    };
    const params = {
      TableName: this.table,
      Item: item,
    };

    await this.create(params);

    return item;
  }

  /**
   *
   * @param {String} eventId
   * @param {String} userId
   * @returns {Promise}
   */
  async getEvent(eventId, userId) {
    const params = {
      TableName: this.table,
      Key: {
        UserId: userId,
        UUID: eventId,
      },
    };

    const { Item } = await this.get(params);
    return Item;
  }

  /**
   *
   * @param {String} id
   * @param {String} userId
   * @returns {Promise}
   */
  async deleteEvent(eventId, userId) {
    const params = {
      TableName: this.table,
      Key: {
        UserId: userId,
        UUID: eventId,
      },
    };

    return this.delete(params);
  }

  /**
   *
   * @param {String} eventId
   * @param {String} userId
   * @param {Object} event
   * @returns {Promise}
   */
  async updateEvent(eventId, userId, event) {
    let updateExpression = 'SET ';
    const expressionAttributeValuesObj = {};
    const expressionAttributeNamesObj = {};

    Object.keys(event).forEach(key => {
      updateExpression += `#${key} = :${key}, `;
      expressionAttributeNamesObj[`#${key}`] = key;
      expressionAttributeValuesObj[`:${key}`] = event[key];
    });

    // Set modified timestamp
    const updatedAt = new Date().getTime();
    updateExpression += '#updatedAt = :updatedAt';
    expressionAttributeNamesObj['#updatedAt'] = 'updatedAt';
    expressionAttributeValuesObj[':updatedAt'] = updatedAt;

    const params = {
      TableName: this.table,
      Key: {
        UserId: userId,
        UUID: eventId,
      },
      ExpressionAttributeNames: expressionAttributeNamesObj,
      ExpressionAttributeValues: expressionAttributeValuesObj,
      UpdateExpression: updateExpression,
      ReturnValues: DDB_RETURN_VALUES.ALL_NEW,
    };

    const updatedAttrs = await this.update(params);
    const updatedEvent = updatedAttrs && updatedAttrs.Attributes ? updatedAttrs.Attributes : {};
    return updatedEvent;
  }

  /**
   *
   * @param {String} eventId
   * @param {String} userId
   * @returns {Promise<Bool>}
   */
  async checkIfEventExists(eventId, userId) {
    const params = {
      TableName: this.table,
      Key: {
        UserId: userId,
        UUID: eventId,
      },
      ProjectionExpression: '#id',
      ExpressionAttributeNames: {
        '#id': 'UUID',
      },
    };

    const { Item } = await this.get(params);
    return !!Item;
  }

  /**
   *
   * @param {String} userId
   * @returns {Promise<Array<Object>>}
   */
  async getAllEvents(userId) {
    const params = {
      ExpressionAttributeValues: {
        ':userId': userId,
      },
      KeyConditionExpression: 'UserId = :userId',
      TableName: this.table,
    };

    const { Items = [] } = await this.query(params);
    return Items;
  }
}

export default EventsRepository;
