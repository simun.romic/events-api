// eslint-disable-next-line import/no-extraneous-dependencies
import AWSXRay from 'aws-xray-sdk';

import axios from 'axios';
import https from 'https';

import { HTTP_REQUEST_METHOD } from '../utils/constants';

AWSXRay.captureHTTPsGlobal(https);

class HttpService {
  constructor() {
    this.httpsAgent = new https.Agent({
      keepAlive: true,
      keepAliveMsecs: 10000000,
      rejectUnauthorized: false,
    });

    this.axiosInstance = axios.create({
      timeout: 30000, // 30 s
      httpsAgent: this.httpsAgent,
      maxContentLength: 5 * 1000 * 1000, // 5MB
    });
  }

  /**
   *
   * @param {*} url
   * @param {*} data
   * @param {*} options
   * @param {*} httpRequestMethod
   * @returns {Promise}
   * @throws {Error}
   */
  async makeRequest(url, data, options, httpRequestMethod) {
    switch (httpRequestMethod) {
      case HTTP_REQUEST_METHOD.GET:
        return this.axiosInstance.get(url, options);
      case HTTP_REQUEST_METHOD.POST:
        return this.axiosInstance.post(url, data, options);
      case HTTP_REQUEST_METHOD.PUT:
        return this.axiosInstance.put(url, data, options);
      case HTTP_REQUEST_METHOD.PATCH:
        return this.axiosInstance.patch(url, data, options);
      case HTTP_REQUEST_METHOD.DELETE:
        return this.axiosInstance.delete(url, options);
      case HTTP_REQUEST_METHOD.HEAD:
        return this.axiosInstance.head(url, options);
      case HTTP_REQUEST_METHOD.OPTIONS:
        return this.axiosInstance.options(url, options);
      default:
        throw new Error(`Unsupported HTTP request type: ${httpRequestMethod}`);
    }
  }

  /**
   * close https agent opened sockets
   * @returns {Promise}
   */
  destroyHttpResources() {
    return new Promise((resolve, reject) => {
      try {
        if (this.httpsAgent) {
          this.httpsAgent.destroy();
        }

        resolve();
      } catch (error) {
        console.error(`Error while closing http(s)Agent sockets: ${error}`);
        reject(error);
      }
    });
  }
}

export default new HttpService();
