class EnrichedError extends Error {
  constructor(message, contextData) {
    super(message);
    this.contextData = contextData;
  }
}

export default EnrichedError;
