import generateIAMPolicy from '../../libs/generators/policyGenerator';

const USER_ID_REGEX = /([0-9]{6}.[a-zA-Z0-9]{32}.[0-9]{4})/;

/**
 *
 * @param {String} userId
 * @returns {Boolean}
 */
const isUserIdValid = userId => {
  return USER_ID_REGEX.test(userId) && userId.length === 44;
};

/**
 *
 * @param {Object} event
 * @returns {Promise<Object>}
 * @throws {Error}
 */
const handler = async event => {
  try {
    const { authorizationToken, methodArn } = event;

    if (!authorizationToken) {
      throw new Error('Unauthorized');
    }

    const isValid = isUserIdValid(authorizationToken);
    if (!isValid) {
      console.log(`Invalid userId token: ${authorizationToken}`);
      throw new Error('Unauthorized');
    }

    const effect = 'Allow';
    const allowAll = true;
    return generateIAMPolicy(authorizationToken, effect, methodArn, allowAll);
  } catch (error) {
    console.log(`Error while authorizing user: ${error}`);
    throw new Error('Unauthorized');
  }
};

export default handler;
