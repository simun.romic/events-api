import generateResponse from '../../libs/utils/responseUtils';
import { STATUS_CODE, CONTENT_TYPE } from '../../libs/utils/constants';

import EventsRepository from '../../libs/repositories/EventsRepository';

const { EVENTS_TABLE } = process.env;

const eventsRepository = new EventsRepository(EVENTS_TABLE);

const responseHeaders = {
  'Content-Type': CONTENT_TYPE.APPLICATION_JSON,
  'Access-Control-Allow-Origin': '*',
};

/**
 *
 * @param {Object} event
 * @returns {Promise<Object>}
 */
const handler = async event => {
  try {
    const { headers } = event;
    const { Authorization } = headers;

    const events = await eventsRepository.getAllEvents(Authorization);

    return generateResponse(STATUS_CODE.OK, responseHeaders, events);
  } catch (error) {
    console.log(`Error while getting event: ${error}`);
    return generateResponse(STATUS_CODE.INTERNAL_SERVER_ERROR, responseHeaders);
  }
};

export default handler;
