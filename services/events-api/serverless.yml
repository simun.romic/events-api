service: events-api

plugins:
  - serverless-bundle
  - serverless-stage-manager
  - serverless-iam-roles-per-function
  - serverless-offline-ssm
  - serverless-offline

custom:
  stages: ${file(../../serverless.common.yml):custom.stages}
  stage: ${file(../../serverless.common.yml):custom.stage}
  AWS_NODEJS_CONNECTION_REUSE_ENABLED: ${file(../../serverless.common.yml):custom.AWS_NODEJS_CONNECTION_REUSE_ENABLED}
  serverless-iam-roles-per-function:
    defaultInherit: ${file(../../serverless.common.yml):custom.serverless-iam-roles-per-function-inherit}
  eventsTableName:
    'Fn::ImportValue': ${self:custom.stage}-PDXEventAlerts
  eventsTableArn:
    'Fn::ImportValue': ${self:custom.stage}-EventsTableArn
  authorizerArn:
    'Fn::ImportValue': ${self:custom.stage}-AuthorizerLambdaFunction

package:
  individually: true

provider:
  name: aws
  runtime: nodejs12.x
  stage: ${opt:stage, 'dev'}
  region: us-west-2
  tracing:
    apiGateway: true
    lambda: true
  environment:
    AWS_NODEJS_CONNECTION_REUSE_ENABLED: ${self:custom.AWS_NODEJS_CONNECTION_REUSE_ENABLED}
    EVENTS_TABLE: ${self:custom.eventsTableName}
  iamRoleStatements:
    - Effect: Allow
      Action:
        - xray:PutTraceSegments
        - xray:PutTelemetryRecords
      Resource: '*'

functions:
  getEvent:
    handler: getEvent.default
    timeout: 10 # 10 s
    memorySize: 256
    events:
      - http:
          path: events/{id}
          request:
            parameters:
              paths:
                id: true
          method: get
          authorizer: ${cf:authorization-api-${self:custom.stage}.AuthorizerLambdaFunctionArn}
          cors: true
    iamRoleStatements:
      - Effect: Allow
        Action:
          - dynamodb:Query
          - dynamodb:Scan
          - dynamodb:GetItem
        Resource: ${self:custom.eventsTableArn}

  createEvent:
    handler: createEvent.default
    timeout: 10 # 10 s
    memorySize: 256
    events:
      - http:
          path: events
          method: post
          authorizer: ${cf:authorization-api-${self:custom.stage}.AuthorizerLambdaFunctionArn}
          cors: true
    iamRoleStatements:
      - Effect: Allow
        Action:
          - dynamodb:PutItem
          - dynamodb:ConditionCheck
        Resource: ${self:custom.eventsTableArn}

  updateEvent:
    handler: updateEvent.default
    timeout: 10 # 10 s
    memorySize: 256
    events:
      - http:
          path: events/{id}
          request:
            parameters:
              paths:
                id: true
          method: put
          authorizer: ${cf:authorization-api-${self:custom.stage}.AuthorizerLambdaFunctionArn}
          cors: true
    iamRoleStatements:
      - Effect: Allow
        Action:
          - dynamodb:PutItem
          - dynamodb:UpdateItem
          - dynamodb:GetItem
          - dynamodb:ConditionCheck
        Resource: ${self:custom.eventsTableArn}

  deleteEvent:
    handler: deleteEvent.default
    timeout: 10 # 10 s
    memorySize: 256
    events:
      - http:
          path: events/{id}
          request:
            parameters:
              paths:
                id: true
          method: delete
          authorizer: ${cf:authorization-api-${self:custom.stage}.AuthorizerLambdaFunctionArn}
          cors: true
    iamRoleStatements:
      - Effect: Allow
        Action:
          - dynamodb:DeleteItem
          - dynamodb:GetItem
          - dynamodb:Query
          - dynamodb:ConditionCheck
        Resource:
          - ${self:custom.eventsTableArn}

  getAllEvents:
    handler: getAllEvents.default
    timeout: 15 # 15 s
    memorySize: 256
    events:
      - http:
          path: events
          request:
            parameters:
              paths:
                id: true
          method: get
          authorizer: ${cf:authorization-api-${self:custom.stage}.AuthorizerLambdaFunctionArn}
          cors: true
    iamRoleStatements:
      - Effect: Allow
        Action:
          - dynamodb:Query
          - dynamodb:Scan
        Resource: ${self:custom.eventsTableArn}
