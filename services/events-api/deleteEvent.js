import generateResponse from '../../libs/utils/responseUtils';
import { STATUS_CODE, CONTENT_TYPE } from '../../libs/utils/constants';

import EventsRepository from '../../libs/repositories/EventsRepository';

const { EVENTS_TABLE, EVENTS_USERS_USER_ID_IDX } = process.env;

const eventsRepository = new EventsRepository(EVENTS_TABLE, EVENTS_USERS_USER_ID_IDX);

const responseHeaders = {
  'Content-Type': CONTENT_TYPE.APPLICATION_JSON,
  'Access-Control-Allow-Origin': '*',
};

/**
 *
 * @param {Object} event
 * @returns {Promise<Object>}
 */
const handler = async event => {
  try {
    const { pathParameters, headers } = event;
    const { id } = pathParameters;
    const { Authorization } = headers;

    const eventExits = await eventsRepository.checkIfEventExists(id, Authorization);

    if (!eventExits) {
      return generateResponse(STATUS_CODE.FORBIDDEN, responseHeaders);
    }

    await eventsRepository.deleteEvent(id, Authorization);

    return generateResponse(STATUS_CODE.OK, responseHeaders);
  } catch (error) {
    console.log(`Error while deleting event: ${error}`);
    return generateResponse(STATUS_CODE.INTERNAL_SERVER_ERROR, responseHeaders);
  }
};

export default handler;
