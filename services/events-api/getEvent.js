import generateResponse from '../../libs/utils/responseUtils';
import { STATUS_CODE, CONTENT_TYPE } from '../../libs/utils/constants';

import EventsRepository from '../../libs/repositories/EventsRepository';

const { EVENTS_TABLE } = process.env;

const eventsRepository = new EventsRepository(EVENTS_TABLE);

const responseHeaders = {
  'Content-Type': CONTENT_TYPE.APPLICATION_JSON,
  'Access-Control-Allow-Origin': '*',
};

/**
 *
 * @param {Object} event
 * @returns {Promise<Object>}
 */
const handler = async event => {
  try {
    const { pathParameters, headers } = event;
    const { id } = pathParameters;
    const { Authorization } = headers;

    const eventObj = await eventsRepository.getEvent(id, Authorization);

    if (!eventObj) {
      return generateResponse(STATUS_CODE.NOT_FOUND, responseHeaders);
    }

    return generateResponse(STATUS_CODE.OK, responseHeaders, { ...eventObj });
  } catch (error) {
    console.log(`Error while getting event: ${error}`);
    return generateResponse(STATUS_CODE.INTERNAL_SERVER_ERROR, responseHeaders);
  }
};

export default handler;
