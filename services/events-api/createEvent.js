import generateResponse from '../../libs/utils/responseUtils';
import { STATUS_CODE, CONTENT_TYPE } from '../../libs/utils/constants';

import EventsRepository from '../../libs/repositories/EventsRepository';

const { EVENTS_TABLE } = process.env;

const eventsRepository = new EventsRepository(EVENTS_TABLE);

const responseHeaders = {
  'Content-Type': CONTENT_TYPE.APPLICATION_JSON,
  'Access-Control-Allow-Origin': '*',
};

/**
 *
 * @param {Object} event
 * @returns {Promise<Object>}
 */
const handler = async event => {
  try {
    const { body, headers } = event;
    const { Authorization } = headers;

    const eventObj = JSON.parse(body);

    const createdEvent = await eventsRepository.createEvent(eventObj, Authorization);
    return generateResponse(STATUS_CODE.OK, responseHeaders, { ...createdEvent });
  } catch (error) {
    console.log(`Error while creating event: ${error}`);
    return generateResponse(STATUS_CODE.INTERNAL_SERVER_ERROR, responseHeaders);
  }
};

export default handler;
