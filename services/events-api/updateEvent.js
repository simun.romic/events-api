import generateResponse from '../../libs/utils/responseUtils';
import { STATUS_CODE, CONTENT_TYPE } from '../../libs/utils/constants';

import EventsRepository from '../../libs/repositories/EventsRepository';

const { EVENTS_TABLE } = process.env;

const eventsRepository = new EventsRepository(EVENTS_TABLE);

const responseHeaders = {
  'Content-Type': CONTENT_TYPE.APPLICATION_JSON,
  'Access-Control-Allow-Origin': '*',
};

/**
 *
 * @param {Object} event
 * @returns {Promise<Object>}
 */
const handler = async event => {
  try {
    const { body, pathParameters, headers } = event;
    const { id } = pathParameters;
    const { Authorization } = headers;

    if (!id) {
      return generateResponse(STATUS_CODE.BAD_REQUEST, responseHeaders);
    }

    const eventExists = await eventsRepository.checkIfEventExists(id, Authorization);
    if (!eventExists) {
      return generateResponse(STATUS_CODE.NOT_FOUND, responseHeaders);
    }

    const eventPayload = JSON.parse(body);
    const createdEvent = await eventsRepository.updateEvent(id, Authorization, eventPayload);
    return generateResponse(STATUS_CODE.OK, responseHeaders, { ...createdEvent });
  } catch (error) {
    console.log(`Error while updating event: ${error}`);
    return generateResponse(STATUS_CODE.INTERNAL_SERVER_ERROR, responseHeaders);
  }
};

export default handler;
